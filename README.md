# Prototype random selection app

For Jonathan Wong

## Deployed on Heroku

This web app is deployed on Heroku [Here](https://secure-anchorage-15545.herokuapp.com/)

## Running the Random Selector code locally

1. Download or copy paste the code from [Here](https://bitbucket.org/johnjchung/jwong_app/src/master/randomselector.py)

2. Do the same with the countries.csv file [Here](https://bitbucket.org/johnjchung/jwong_app/src/master/countries.csv)

3. Install python on your PC and execute this command on your powershell or terminal and make sure you're in the same directory as the 2 files above:

`python randomselector.py`

OR you can do it the long way:

1. Install python on your PC

2. Install git on your PC

3. On your desktop or a directory of your choice, copy and paste these commands on either your powershell or terminal in this order:

`git clone https://johnjchung@bitbucket.org/johnjchung/jwong_app.git`

`cd jwong_app`

`python randomselector.py`

## Quick Start for running the web application locally

To get this project up and running locally on your computer:
1. Set up the [Python development environment](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/development_environment).
   Recommend using a Python virtual environment.

2. Assuming you have Python setup, run the following commands (if you're on Windows you may use `py` or `py -3` instead of `python` to start Python):
   ```
   pip3 install -r requirements.txt
   python3 manage.py makemigrations
   python3 manage.py migrate
   python3 manage.py collectstatic
   python3 manage.py test # Run the standard tests. These should all pass.
   python3 manage.py createsuperuser # Create a superuser
   python3 manage.py runserver
   ```
3. Open a browser to `http://127.0.0.1:8000/admin/` to open the admin site

4. Create a few test objects of each type.

5. Open tab to `http://127.0.0.1:8000` to see the main site, with your new objects.
