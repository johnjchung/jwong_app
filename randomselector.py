

'''
Application to randomly select from a list or a file data structure

'''

import pandas as pd
import numpy as np
import csv
import random

with open("countries.csv") as f:
    reader = csv.reader(f)
    chosen_row = random.choice(list(reader))

print("Printing chosen row: ", chosen_row)
print("Printing the country: ", chosen_row[3])
